class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy

  def index
    @users = User.paginate(page: params[:page])
  end

  def new
  	@user = User.new
  end

  def show 
  	@user = User.find params[:id]
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def show_an_user_by_email
  	@user = User.find_by email: params[:email].downcase+"@gmail.com"
  end

  # Defines a proto-feed.
  # See "Following users" for the full implementation.
  def feed
    Micropost.where("user_id = ?", id)
  end

  def create
    @user = User.new user_params
    #date_params = params[:birthday]
    #@user.birthday = Date.new(date_params[:year].to_i, date_params[:month].to_i, date_params[:day].to_i)
  	if @user.save
  		#log_in @user
  		UserMailer.account_activation(@user).deliver_now
  		flash[:success] = "Welcome to the Sample App!"
  		redirect_to user_url(@user)
  	else
  		render 'new'
  	end
  end

  def edit
      @user = User.find(params[:id])
  end

  	def update
	    @user = User.find(params[:id])
	    if @user.update_attributes(user_params)
	      	flash[:success] = "Profile updated"
      		redirect_to @user
	    else
	      render 'edit'
	    end
 	end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

 	private
	    def user_params
	      params.require(:user).permit(:name, :email, :password, :password_confirmation, :gender, :birthday)
	    end

      # Confirms a logged-in user.
      # def logged_in_user
      #   unless logged_in?
      #     store_location
      #     flash[:danger] = "Please log in."
      #     redirect_to login_url
      #   end
      # end

      # Confirms the correct user.
      def correct_user
        @user = User.find(params[:id])
        redirect_to(root_url) unless current_user?(@user)
      end

      # Confirms an admin user.
      def admin_user
        redirect_to(root_url) unless current_user.admin?
      end
end
